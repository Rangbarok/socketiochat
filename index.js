var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var _ = require('lodash');
// const conf = {port: 7003, ttl: 800000, lockRequestTimeout: 2000, lockRetryMax: 10};
// const lmUtils = require('live-mutex').lmUtils;

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/register.html');
});

app.get('/chat', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

var connectedClients = ['general'];

io.on('connection', function (socket) {

  socket.on('emitNewConn', function (name) {
    console.log('Emitting new user join the chat... ', name)
    io.sockets.emit('broadcast', '' + name + ' join the app');
  });


  socket.on('register', function (clientName, fn) {
    console.log("New register attempt with " + clientName + ' username...')
    var registerOk = false;
    if (!_.includes(connectedClients, clientName)) {
      registerOk = true;
      connectedClients.push(clientName);
    }
    fn(registerOk);
  });

  socket.on('leave-room', function(room) {
    console.log('Leaving room ', room);
    socket.leave(room);
  });

  socket.on('remove client', function (name) {
    console.log("New user desconnection... ", name);
    _.remove(connectedClients, function (client) {
      return client === name;
    });
    io.emit('updateClients', connectedClients);
    io.sockets.emit('broadcast', '' + name + ' leave the app');
  });

  socket.on('refreshClients', function () {
    console.log("Updating connecetd clients... ");
    io.emit('updateClients', connectedClients);
  });

  socket.on('chat message', function (data) {
    console.log('New message from a client');
    if (data.room && data.room !== 'general') {
      console.log("emiting to room :: ", data);
      io.to(data.room).emit('room-mess', data.message);
    } else {
      io.emit('chat message', data);
    }
  });

  socket.on('join-room', function (room) {
    console.log('Joining room ' + room + '...');
    socket.join(room);
  });

});

http.listen(port, function () {
  console.log('listening on *:' + port);
  // lmUtils.launchBrokerInChildProcess(conf, function (err) {
  //   console.log('BROKER::::', err);
  // });
});
